# Curriculum Vitae

## Table of content

* [How to run](#how-to-run)
* [Download Files](#download-files)
* [Final Results](#final-results)
* [License and credits](#license-and-credits)

![Logo Image](./src/images/profile.png)

## How to run

To run cv simply use the command `make` in the terminal.

## Download Files

Here are the links to download the docs:
- [cv.pdf][1]
- [coverletter.pdf][2]

There is also a `french` version [here][3].

## Final Results

| Curriculum Vitae | Coverletter |
|:---:|:---:|
|[![Curriculum Vitae marckit](src/images/docs/cv.jpg "Curriculum Vitae")*My Curriculum Vitae*][4]|[![Coverletter marckit](src/images/docs/cv.jpg "Coverletter")*My Coverletter*][5]|

## License and credits

This [curriculum vitae][6] was inspired by [muratcankaracabey][7].
This project is under the license of [MIT](./LICENSE).

<!--- Download links -->
[1]:https://gitlab.com/marckit/cv-marckit/-/raw/main/pdf/cv.pdf?inline=false
[2]:https://gitlab.com/marckit/cv-marckit/-/raw/main/pdf/coverletter.pdf?inline=false
[3]:https://gitlab.com/marckit/cv-marckit/tree/french
[4]:src/images/docs/cv.jpg
[5]:src/images/docs/coverletter.jpg
[6]:https://gitlab.com/muratcankaracabey/latex_cv/
[7]:https://gitlab.com/muratcankaracabey/
